using UnityEngine;

namespace Runner3d.Scripts.Obstacles
{
    public class HorizontalObstacle : MonoBehaviour
    {
        [Header("Obstacle movement variables")]
        [SerializeField, Range(1f, 5f)] private float MovingDuration = .5f;// Can be changed via editor which is 1/proportional with obstacle speed
        [Header("Moving Direction")]
        [SerializeField] private bool ismovingHorizontal = true;//In my level design, all moving obstacles are horizontal, however for further levels this boolean can be check for moving verical.
        private static float upperbound=8f; 
        private static float loverbound=0f; 

        void Start()
        {
            if (ismovingHorizontal)
                MoveHorizontal();
            else
                MoveVertical();
            
        }
        void MoveHorizontal()
        {
       
            transform.position = new Vector3(InGameVariables.CharacterLeftbound , transform.position.y, transform.position.z);//initialize the starting point of obstacle
            LeanTween.moveX(this.gameObject, InGameVariables.CharacterRightbound , MovingDuration).setLoopPingPong();
        }
        void MoveVertical()
        {
            transform.RotateAround(transform.position, Vector3.forward, 90f);
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * 2, transform.localScale.z);
            transform.position = new Vector3(0f, upperbound, transform.position.z);//initialize the starting point of obstacle
            LeanTween.moveY(this.gameObject, loverbound, MovingDuration).setLoopPingPong();
        }

    }
}

