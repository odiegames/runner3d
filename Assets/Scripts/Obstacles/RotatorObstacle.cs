using UnityEngine;

namespace Runner3d.Scripts.Obstacles
{
    public class RotatorObstacle : MonoBehaviour
    {
        [Header("Rotating around variables")]
        [SerializeField, Range(3f, 6f)] private float RotateAroundDuration = 4f;
        void Start()
        {
            RotateAround();
        }
        void RotateAround()
        {
            LeanTween.rotateAround(this.gameObject, Vector3.up, 360f, RotateAroundDuration).setLoopClamp();
        }

    }

}
