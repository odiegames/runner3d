using UnityEngine;
using Runner3d.Scripts.Characters;

public class RotatingPlatform : MonoBehaviour
{
    [Header("Applying force variables")]
    [SerializeField, Range(0f, 30f)] private float Forcemultiplier = 5f;
    [Header("Rotating around variables")]
    [SerializeField, Range(3f, 6f)] private float RotateAroundDuration = 4f;
    [SerializeField] private bool Clockwiserotation = false; //this boolean indicates the direction of force and rotation

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var CharacterScript = other.gameObject.GetComponent<OpponentAI>();
            if (CharacterScript != null)
            {
                if (name == "FirstPlatform")
                    CharacterScript.SetPositionForRotatingPlatformEnter();
                CharacterScript.OnEnterRotatingPlatform((Clockwiserotation ? 1 : -1)* Forcemultiplier);//clockwiserotation means applying  force in right direction to player
            }
               
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var CharacterScript = other.gameObject.GetComponent<OpponentAI>();
            if (CharacterScript != null && name=="ThirdPlatform")
                CharacterScript.OnExitRotatingPlatform();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var playerRigidBody = other.gameObject.GetComponent<Rigidbody>();
            if (playerRigidBody != null)
                ApplyForceToPlayer(playerRigidBody);

        }
    }
    void Start()
    {
        RotateAround();
    }
    void RotateAround()
    {
        LeanTween.rotateAround(this.gameObject, (Clockwiserotation ?  Vector3.back : Vector3.forward ), 360f , RotateAroundDuration).setLoopClamp();
    }
    private void ApplyForceToPlayer(Rigidbody playerrb)
    {
       
        playerrb.AddForce((Clockwiserotation ? Vector3.right : Vector3.left) * Forcemultiplier);
    }
}
