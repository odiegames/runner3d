using UnityEngine;

namespace Runner3d.Scripts.Obstacles
{
    public class HalfDonutObstacle : MonoBehaviour
    {
        [Header("Moving Stick Variables")]
        [SerializeField] private GameObject movingStick;
        private static float movingstickhitpositionx = -.35f;
        private static float movingsticidlepositionx = 0f;
        
        void Start()
        {
            MoveStick();
        }

        private void MoveStick()
        {
            var seq = LeanTween.sequence();
            seq.append(LeanTween.moveLocalX(movingStick, movingstickhitpositionx, .25f));
            seq.append(LeanTween.moveLocalX(movingStick, movingsticidlepositionx, .5f));
            seq.append(LeanTween.delayedCall(Random.Range(1f, 3f), MoveStick));
        }
    }
}

