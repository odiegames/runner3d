using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Runner3d.Scripts.Core
{
    public enum GameState
    {
        PrePlay,
        PlayRun,
        PlayPaint,
        PreLose,//this state is for future rewarded add feature
        Lose,   //this state is for future Lose state(for now no Lose)
        LevelCompleted,
        PrePaint
    }
    public class LevelManager : MonoBehaviour
    {
        #region SİNGLETON ASSIGNMENT
        public static LevelManager INSTANCE;
        private void Awake()
        {
            if (INSTANCE != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                INSTANCE = this;
            }
        }
        #endregion
        [SerializeField]private GameState gamestatus=GameState.PrePlay;
        public GameState GameStatus
        {
            get { return gamestatus; }
            set
            {
                if (value == gamestatus)
                    return;
                switch (value)
                {
                    case GameState.PlayRun:
                        Actions.OnStartGame?.Invoke();
                        break;
                    case GameState.PrePaint:
                        Actions.OnPrePaintState?.Invoke();
                        break;
                    case GameState.PlayPaint:
                        Actions.OnPaintState?.Invoke();
                        break;
                    case GameState.LevelCompleted:
                        Actions.OnLevelCompletedState?.Invoke();
                        break;

                }



                gamestatus = value;
            }
        }

        [Header("The Rank Stuffs")]
        public Transform MainCharacterposition;
        public List<Transform> Opponentspositions = new List<Transform>();
        public int MainCharacterRank = 11;
        private void FixedUpdate()
        {
            CalculateCurrentRank();
        }
        public void CalculateCurrentRank()
        {
            if (GameStatus != GameState.PlayRun)
                return;
            var frontOpponents = Opponentspositions.Where(i => i.position.z > MainCharacterposition.position.z);
            MainCharacterRank = frontOpponents.Count() + 1;
        }

        [Header("The Paint Stuffs")]
        private int paintingWallPercentage;
        public int PaintingWallPercentage
        {
            get { return paintingWallPercentage; }
            set
            {
                if (value == paintingWallPercentage)
                    return;
                paintingWallPercentage = value;
                if (paintingWallPercentage > 80)
                    Actions.CanContinueAfterPainting?.Invoke();
            }

        }

    }
    
}

