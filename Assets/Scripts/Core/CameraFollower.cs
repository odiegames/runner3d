using UnityEngine;
using Runner3d.Scripts.Characters;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private Character character; // camera following character
    private Vector3 CameraOffsettoCharacter;
    void Start()
    {
        CameraOffsettoCharacter = transform.position - character.gameObject.transform.position;
    }

    private void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x,transform.position.y,CameraOffsettoCharacter.z+ character.gameObject.transform.position.z); 
    }
}
