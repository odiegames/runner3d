using UnityEngine;

public static class InGameVariables
{
    public static float CharacterLeftbound = -6f;
    public static float CharacterRightbound = +6f;
    public static Vector3 CharacterStartPoint;
}
