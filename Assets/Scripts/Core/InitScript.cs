using UnityEngine;
namespace Runner3d.Scripts.Core
{
    public class InitScript : MonoBehaviour
    {

        public static InitScript INSTANCE;
        public int Level { get; private set; }
        private void Awake()
        {
            if (INSTANCE != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                INSTANCE = this;
            }
            if (PlayerPrefs.GetInt("Launched") == 0)
            {
                PlayerPrefs.SetInt("Level", 1);
                Level = 1;
                PlayerPrefs.Save();
            }
            else if (PlayerPrefs.GetInt("Launched") == 1)
            {
                Level = PlayerPrefs.GetInt("Level");
            }
        }


    }
}

