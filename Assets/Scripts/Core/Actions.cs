
using System;

public static class Actions
{
    public static Action OnStartGame;
    public static Action CanContinueAfterPainting;
    public static Action OnPrePaintState;
    public static Action OnPaintState;
    public static Action OnLevelCompletedState;
}
