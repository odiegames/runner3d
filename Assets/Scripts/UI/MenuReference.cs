using UnityEngine;
using UnityEngine.SceneManagement;
using Runner3d.Scripts.Core;
namespace Runner3d.Scripts.UI
{
    public class MenuReference : MonoBehaviour
    {
        #region SİNGLETON ASSIGNMENT
        public static MenuReference INSTANCE;
        private void Awake()
        {
            if (INSTANCE != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                INSTANCE = this;
            }
        }
        #endregion

        private void OnEnable()
        {
            Actions.OnStartGame += OnRunState;
            Actions.OnPrePaintState += OnPrePaintState;
            Actions.OnPaintState += OnPaintState;
            Actions.OnLevelCompletedState += OnLevelCompleted;
            Actions.CanContinueAfterPainting += CanContinueAfterPainting;
        }
        private void OnDisable()
        {
            Actions.OnStartGame -= OnRunState;
            Actions.OnPrePaintState -= OnPrePaintState;
            Actions.OnPaintState -= OnPaintState;
            Actions.OnLevelCompletedState -= OnLevelCompleted;
            
        }
        [SerializeField] private GameObject PrePlayPanel;
        [SerializeField] private GameObject RunningStatePanel;
        [SerializeField] private GameObject PaintingStatePanel;
        [SerializeField] private GameObject PaintingStateContinueButton;
        [SerializeField] private GameObject PrePaintingPanel;
        [SerializeField] private GameObject LevelCompletedPanel;

        private void Start()
        {
            if (!PrePlayPanel.activeSelf)
                PrePlayPanel.SetActive(true);
            if (RunningStatePanel.activeSelf)
                RunningStatePanel.SetActive(false);
            if (PrePaintingPanel.activeSelf)
                PrePaintingPanel.SetActive(false);
            if (PaintingStatePanel.activeSelf)
                PaintingStatePanel.SetActive(false);
            if (LevelCompletedPanel.activeSelf)
                LevelCompletedPanel.SetActive(false);
        }
        public void StartGame()
        {
            
            LevelManager.INSTANCE.GameStatus = GameState.PlayRun;
        }
        public void StartPaint()
        {
            LevelManager.INSTANCE.GameStatus = GameState.PlayPaint;
        }
        public void ContinueAfterPaint()
        {
            LevelManager.INSTANCE.GameStatus = GameState.LevelCompleted;
        }
        public void OnRunState()
        {
            PrePlayPanel.SetActive(false);
            RunningStatePanel.SetActive(true);
        }
        public void OnPrePaintState()
        {
            RunningStatePanel.SetActive(false);
            PrePaintingPanel.SetActive(true);
        }
        public void OnPaintState()
        {
            PrePaintingPanel.SetActive(false);
            PaintingStateContinueButton.SetActive(false);
            PaintingStatePanel.SetActive(true);
        } 
        public void OnLevelCompleted()
        {
            PaintingStatePanel.SetActive(false);
            LevelCompletedPanel.SetActive(true);
        }
        public void CanContinueAfterPainting()
        {
            PaintingStateContinueButton.SetActive(true);
            Actions.CanContinueAfterPainting -= CanContinueAfterPainting;//Since this is for once, this function should not listen for action!
        }
        public void PlayAgain()
        {
            LeanTween.cancelAll();
            SceneManager.LoadScene("SampleScene");
        }
    }
}

