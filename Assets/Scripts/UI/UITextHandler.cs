using UnityEngine;
using TMPro;
using Runner3d.Scripts.Core;

namespace Runner3d.Scripts.UI
{
    public class UITextHandler : MonoBehaviour
    {
        TextMeshProUGUI textUI;
        void Start()
        {
            textUI = GetComponent<TextMeshProUGUI>();
        }

        // Update is called once per frame
        void Update()
        {
            if (name == "Rank")
            {
                textUI.text = LevelManager.INSTANCE.MainCharacterRank.ToString();
            }
            if (name == "PaintedAmount")
            {
                textUI.text = "% " + LevelManager.INSTANCE.PaintingWallPercentage.ToString();
            }
            if (name == "LevelText")
            {
                textUI.text = "Level " + InitScript.INSTANCE.Level.ToString();
            }
        }
    }

}
