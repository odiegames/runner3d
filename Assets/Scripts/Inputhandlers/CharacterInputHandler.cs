using UnityEngine;
using Runner3d.Scripts.Core;

public class CharacterInputHandler : MonoBehaviour
{
    [Header("Character follow Camera")]
    [SerializeField] private Camera cam;

    [Header("Input variables")]
    [SerializeField, Range(0f, 1000f)] private float sensitivityMultiplier = 2f;//this is for the sensitivity of input 
    private Vector2 MouseClickPosition;
    private Vector2 MouseCurrentPosition;
    private bool isInputPressing = false;

    private Vector3 CharacterClickPosition;

    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        HandleInput();
        
    }
    private void FixedUpdate()
    {
        MoveCharacter();
    }
    void HandleInput()
    {
        if (LevelManager.INSTANCE.GameStatus != GameState.PlayRun)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            SetReferencePoint();
            isInputPressing = true;
        }
        if (Input.GetMouseButton(0))
        {
            MouseCurrentPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            ResetDrag();
        }
    }
    
    void MoveCharacter()
    {
        if (LevelManager.INSTANCE.GameStatus != GameState.PlayRun)
            return;
        if (isInputPressing)
        {
            float Inputdifferencex = (MouseCurrentPosition.x - MouseClickPosition.x) / cam.pixelWidth;
            float CharacterNextXPosition = CharacterClickPosition.x + (Inputdifferencex * sensitivityMultiplier * Time.deltaTime);
            if (CharacterNextXPosition > InGameVariables.CharacterRightbound)
            {
                SetReferencePoint();
                CharacterNextXPosition = InGameVariables.CharacterRightbound;
            }
            else if (CharacterNextXPosition < InGameVariables.CharacterLeftbound)
            {
                SetReferencePoint();
                CharacterNextXPosition = InGameVariables.CharacterLeftbound;
            }
            rb?.MovePosition(new Vector3(CharacterNextXPosition, transform.position.y, transform.position.z));
        }
    }
    void ResetDrag()
    {
        isInputPressing = false;
        ResetVelocityx();
    }
    public void SetReferencePoint()
    {
        MouseCurrentPosition = MouseClickPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        CharacterClickPosition = transform.position;
        ResetVelocityx();
    }
    void ResetVelocityx()
    {
        if (rb != null)
        {
            rb.velocity = new Vector3(0f, rb.velocity.y, rb.velocity.z);
        }
    }
}
