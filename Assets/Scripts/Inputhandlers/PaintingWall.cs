using UnityEngine;
using Runner3d.Scripts.Core;

namespace Runner3d.Scripts.InputHandlers
{
    public class PaintingWall : MonoBehaviour
    {
        [Header("Painting Stuffs")]
        [SerializeField] private GameObject BrushPrefab;
        [SerializeField] private LineRenderer currentbrush;
        [SerializeField] private RenderTexture PaintingTexture;
        [Header("Input Stuffs")]
        [SerializeField] private Camera maincamera;
        [SerializeField] private LayerMask PaintingWallLayer;
       

        private void FixedUpdate()
        {
            HandleInput();

        }
        private void HandleInput()
        {
            if (LevelManager.INSTANCE.GameStatus != GameState.PlayPaint)
                return;
            if (Input.GetMouseButtonDown(0) && currentbrush.positionCount > 2)
            {
                CreateNewLineRenderer();
            }
            if (Input.GetMouseButton(0))
            {
                RaycastHit hit;
                Ray ray = maincamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100f, PaintingWallLayer))
                {
                    Vector3 hitpoint = hit.point;
                    PaintWall(hitpoint);
                }
            }

        }
        private void PaintWall(Vector3 paintingposition)
        {
            if (currentbrush.positionCount > 2)
            {
                if (Vector3.Magnitude(currentbrush.GetPosition(currentbrush.positionCount - 1) - paintingposition) < .5f)
                    return;
                if (ShouldBrushChange(paintingposition))
                    CreateNewLineRenderer();
            }
            currentbrush.positionCount++;
            currentbrush.SetPosition(currentbrush.positionCount - 1, paintingposition);
            SavePaintedTexture();
        }
        private bool ShouldBrushChange(Vector3 paintingposition)
        {
            Vector3 PreviousPaintedPosition = currentbrush.GetPosition(currentbrush.positionCount - 2);
            Vector3 LastPaintedPosition = currentbrush.GetPosition(currentbrush.positionCount - 1);
            Vector3 previousdirection = (LastPaintedPosition - PreviousPaintedPosition).normalized;
            Vector3 currentdirection = (paintingposition - LastPaintedPosition).normalized;
            float AngleDeg = Vector3.Angle(currentdirection, previousdirection);
            return AngleDeg > 90;
        }
        private void CreateNewLineRenderer()
        {
            GameObject go = Instantiate(BrushPrefab, transform);
            currentbrush = go.GetComponent<LineRenderer>();
        }
        private bool isPixelRed(Color color)
        {
            if (color.r > .5 && color.g < .2 && color.b < .2)
                return true;
            return false;
        }
        private void SavePaintedTexture()
        {
            RenderTexture.active = PaintingTexture;
            var texture2d = new Texture2D(PaintingTexture.width, PaintingTexture.height);
            texture2d.ReadPixels(new Rect(0, 0, PaintingTexture.width, PaintingTexture.height), 0, 0);
            texture2d.Apply();
            var colors = texture2d.GetPixels32();
            int RedPixels = 0;
            float totalPixels = PaintingTexture.width * PaintingTexture.height;
            foreach (Color color in colors)
            {
                if (isPixelRed(color)) RedPixels++;
            }
            LevelManager.INSTANCE.PaintingWallPercentage = Mathf.RoundToInt((RedPixels * 100f) / totalPixels);
        }
    }
}

