using System.Collections;
using UnityEngine;
using Runner3d.Scripts.Core;

namespace Runner3d.Scripts.Characters
{
    public class Character : MonoBehaviour
    {
        [Header("Character variables")]
        [SerializeField] protected Rigidbody rb;
        [SerializeField] private Animator characteranim;
        [SerializeField,Range(0f,15f)] private float SpeedOnZ = 2f;
        private float speedonZ = 0f; 
        protected bool isfinishedrace = false;
        protected bool IsPlayerInsideRotatingPlatform = false;
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("FinishLine"))
            {
                SetCharacterToIdle(true);
                if (this.gameObject.name == "MainPlayer")
                    LevelManager.INSTANCE.GameStatus = GameState.PrePaint;
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("obstacle"))
            {
                GoToStartPoint();
            }
           
        }
        private void OnCollisionStay(Collision collision)
        {
            if (collision.gameObject.CompareTag("HalfDonut") || collision.gameObject.CompareTag("RotatingStick"))
            {
                ApplyForcewithdirection((transform.position - collision.transform.position).normalized,10f);

            }
        }
        private void OnEnable()
        {
            Actions.OnStartGame += SetCharacterToWalk;
            Actions.OnPaintState += SetCharacterToIdle;
            Actions.OnPrePaintState += OnPrePaintState;
        }
        private void OnDisable()
        {
            Actions.OnStartGame -= SetCharacterToWalk;
            Actions.OnPaintState -= SetCharacterToIdle;
            Actions.OnPrePaintState -= OnPrePaintState;
        }

        private void Start()
        {
            SetCharacterToIdle();
            if (this.gameObject.name == "MainPlayer")
                InGameVariables.CharacterStartPoint = transform.position;
            StartCoroutine(CheckCharacterDropCor());
        }
        IEnumerator CheckCharacterDropCor()
        {
            while (true)
            {
                yield return new WaitForSeconds(.5f);
                if (transform.position.y < -5f)//check if the character has dropped
                    GoToStartPoint();
            }
        }
        void Update()
        {
            MoveCharacterToward();
        }
        public void MoveCharacterToward()
        {
            if (LevelManager.INSTANCE.GameStatus != GameState.PlayRun)
                return;
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, speedonZ);

        }
        private void SetCharacterToWalk()
        {
            if (characteranim != null)
            {
                characteranim.SetTrigger("walk");
            }
            speedonZ = SpeedOnZ;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        private void SetCharacterToIdle()
        {
            if (characteranim != null)
            {
                characteranim.SetTrigger("idle");
            }
            speedonZ = 0f;

        }
        private void SetCharacterToIdle(bool forfinish = false)
        {

            if (characteranim != null)
            {
                if (forfinish)
                {
                    if (name != "MainPlayer" || LevelManager.INSTANCE.MainCharacterRank == 1)
                        characteranim.SetTrigger("win");
                    else if (name == "MainPlayer" && LevelManager.INSTANCE.MainCharacterRank != 1)
                        characteranim.SetTrigger("lose");
                    isfinishedrace = true;
                }
                else
                {
                    characteranim.SetTrigger("idle");
                }

            }
            speedonZ = 0f;

        }
        public void GoToStartPoint()
        {
            float StartingXPosition = Random.Range(InGameVariables.CharacterLeftbound, InGameVariables.CharacterRightbound);
            transform.position =  new Vector3(StartingXPosition,InGameVariables.CharacterStartPoint.y,InGameVariables.CharacterStartPoint.z);
            IsPlayerInsideRotatingPlatform = false;
            if (rb != null)
                rb.velocity = Vector3.zero;
        }

        public void OnPrePaintState()
        {
            if (!isfinishedrace)
               GoToStartPoint();

        }
        private void ApplyForcewithdirection(Vector3 NormalVector,float ForceAmount)
        {
            if (rb != null)
            {
                rb.AddForce(NormalVector * 10f, ForceMode.Impulse);
            }
        }
    }
}

