using UnityEngine;

namespace Runner3d.Scripts.Characters
{
    public class OpponentAI : Character
    {

        [Header("AI Parameters")]
        [SerializeField] private float scanangle = 15f;
        [SerializeField] private float ObstacleDetectionDistance = 20f;
        [SerializeField] private LayerMask RaycastlayerMask;
        private static float stuckoffset = 1f;
        private bool isChangingPosition = false;

        [Header("Rotating Platform Stuffs for AI")]
        [SerializeField] private float rotatingPlatformApplyingForce;


        private void Update()
        {
            MoveCharacterToward();
            RotatingPlatformHandle();
            LookForObstacles();
        }
        void LookForObstacles()
        {
            RaycastHit hit;
            Ray rayCenter = new Ray(transform.position, transform.forward);
            Ray rayLeft = new Ray(transform.position, Quaternion.AngleAxis((-1) * scanangle, transform.up) * transform.forward);
            Ray rayRight = new Ray(transform.position, Quaternion.AngleAxis(scanangle, transform.up) * transform.forward);

            if (Physics.Raycast(rayLeft, out hit, ObstacleDetectionDistance, RaycastlayerMask))
            {
                if (!isChangingPosition)
                    ChangePosition(false);

            }
            else if (Physics.Raycast(rayRight, out hit, ObstacleDetectionDistance, RaycastlayerMask))
            {
                if (!isChangingPosition)
                    ChangePosition(true);

            }
            else if (Physics.Raycast(rayCenter, out hit, ObstacleDetectionDistance, RaycastlayerMask))
            {
                if (!isChangingPosition)
                    ChangePosition(Random.Range(0f, 1f) < .5f ? true : false);

            }
        }

        void ChangePosition(bool leftdirection = true)
        {
            float movingxposition = 0f;

            if (leftdirection)
            {
                if (Mathf.Abs(transform.position.x - InGameVariables.CharacterLeftbound) > stuckoffset)
                    movingxposition = Random.Range(transform.position.x, InGameVariables.CharacterLeftbound);
                else
                    movingxposition = Random.Range(transform.position.x, InGameVariables.CharacterRightbound);
            }
            else
            {
                if (Mathf.Abs(transform.position.x - InGameVariables.CharacterRightbound) > stuckoffset)
                    movingxposition = Random.Range(transform.position.x, InGameVariables.CharacterRightbound);
                else
                    movingxposition = Random.Range(transform.position.x, InGameVariables.CharacterLeftbound);
            }

            LeanTween.moveX(this.gameObject, movingxposition, .1f).setOnStart(() => isChangingPosition = true).setOnComplete(() => isChangingPosition = false);
        }
        void RotatingPlatformHandle()
        {

            if (IsPlayerInsideRotatingPlatform)
            {
                if (rb != null)
                {
                    rb.AddForce(Vector3.left * rotatingPlatformApplyingForce);
                }
                
            }
        }
        
        public void OnEnterRotatingPlatform( float ForcefromRotatingPlatform)
        {
            IsPlayerInsideRotatingPlatform = true;
            rotatingPlatformApplyingForce =  ForcefromRotatingPlatform;
        }
        public void SetPositionForRotatingPlatformEnter()
        {
            rb.MovePosition(new Vector3(Random.Range(-1f, 1f), transform.position.y, transform.position.z));
        }
        public void OnExitRotatingPlatform()
        {
            IsPlayerInsideRotatingPlatform = false;
            rb.MovePosition(new Vector3(0f, 0f, transform.position.z));
        }
        
    }
}

